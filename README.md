# IN3230/4230 Plenary session code examples #

This repository will contain various code examples shown or related to
the plenary sessions in the
[IN3230/IN4230](https://www.uio.no/studier/emner/matnat/ifi/IN4230/h19/)
Networking course at the University of Oslo, as taught in the fall of
2019.

## Sessions ##

### Plenary 1: 30.08.2019 ###

Examples can be found in the [basics](basics/) directory.

  * Socket API basics
  * Some C repetition
  * A really simple client/server connection-oriented example
  * We did not have time to look at raw sockets this week, they will
    be introduced in the next session
