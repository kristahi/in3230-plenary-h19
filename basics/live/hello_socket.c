#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/un.h>

void client(int so, struct sockaddr_un *so_name, char *msg);
void server(int so_l, struct sockaddr_un *so_name, char *path);

int main(int argc, char *argv[])
{
  char *path;
  int so;

  
  printf("Hello socket example: I am %s\n", argv[0]);

  path = argv[1];

  so = socket(PF_UNIX, SOCK_STREAM, 0);
  if (so == -1) {
    perror("socket");
    return 1;
  }

  printf("socket fd: %d\n", so);

  struct sockaddr_un so_name;
  so_name.sun_family = PF_UNIX;
  strncpy(so_name.sun_path, path, sizeof(so_name.sun_path) - 1);

  if (strstr(argv[0], "hellod") != NULL) {
    server(so, &so_name, path);
  } else {
    client(so, &so_name, argv[2]);
  }

  return 0;
}

void server(int so_l, struct sockaddr_un *so_name, char *path) {
  int rc;
  int so;
  
  printf("I am a server\n");

  unlink(path);

  rc = bind(so_l, (const struct sockaddr*)so_name, sizeof(struct sockaddr_un));
  if (rc == -1) {
    perror("bind");
    return;
  }

  rc = listen(so_l, 5);
    if (rc == -1) {
    perror("listen");
    return;
  }

    for (;;) {
      so = accept(so_l, NULL, NULL);
      if (so == -1) {
	perror("accept");
	return;
      }

      char buf[256];
      memset(buf, 0, 256);

      rc = read(so, buf, 256);
      if (rc == -1) {
	perror("read");
	return;
      } else if (rc == 0) {
	/* NB: we will never get here if we receive a message
	 * first. The reason is that I didn't have time to implement
	 * reading in a loop, which you should essentially always
	 * do. See the annotated example 'socket_hello.c' in the
	 * parent directory.
	 */
	printf("Client went away\n");
	close(so);
	return;
      }

      printf("%s\n", buf);

      close(so);
    }

}

void client(int so, struct sockaddr_un *so_name, char *msg) {
  int rc;

  rc = connect(so, (const struct sockaddr*)so_name, sizeof(struct sockaddr_un));
  if (rc == -1)
    perror("connect");

  rc = write(so, msg, strlen(msg) + 1);
  if (rc == -1)
    perror("write");
}
