# First plenary session #

## Basic socket API ##

We implemented a very simple client/server program using UNIX domain
sockets.

Source code of the program we wrote live during the session:
[live/hello_socket.c](live/hello_socket.c)
See [socket_hello.c](socket_hello.c) for an annotated, cleaner version of
the same example.

We didn't have time to look at this during the plenary, but with a few
simple changes, we can modify it to use TCP instead: [tcp_hellod.c](tcp_hellod.c)

### Running the examples ###

We have provided a makefile to compile the examples, so just run
`make` in either this directory or the `live` subdirectory.

The UNIX socket example is run as such follows.
server: `./hellod socket_name` (quit with CTRL+C)
client: `./helloc socket_name message`

The TCP example is run like this:
server: `./tcp_hellod portnumber`
client: `./tcp_helloc portnumber message`

## Netcat: the networking swiss army knife

We very briefly demonstrated the netcat (or nc, for short) utility for
interacting with socket and network applications.

See `man nc`.

You can connect to the UNIX domain server we made like so:
`nc -U socketname` and type messages in the console.
Hit CTRL+C to quit.

To connect to the TCP server: `nc localhost portnumber` and as above.
