#include <stdlib.h> 		/* malloc */
#include <stdio.h>		/* printf */
#include <string.h>		/* memset, strcmp, strncpy */
#include <sys/socket.h>		/* socket, bind, listen, accept */
#include <sys/un.h>		/* struct sockaddr_un */
#include <unistd.h>		/* read, close, unlink */

#define BUF_SIZE 256

void server(int l_so, struct sockaddr_un *so_name);
void client(int so, struct sockaddr_un *so_name, char *msg);

int main(int argc, char *argv[])
{
  int so, rc;
  struct sockaddr_un so_name;
  
  /* Create socket */
  so = socket(AF_UNIX, SOCK_STREAM, 0);
  if (so == -1) {
    perror("socket");
    return 1;
  }

  /* Zero out name struct */
  memset(&so_name, 0, sizeof(struct sockaddr_un));

  /* Prepare UNIX socket name */
  so_name.sun_family = AF_UNIX;
  strncpy(so_name.sun_path, argv[1], sizeof(so_name.sun_path) - 1);

  /* The rest depends on whether we should act as server or client */
  printf("I am %s\n", argv[0]);
  
  if (strstr(argv[0], "hellod") != NULL) {
    /* Delete the socket 'file' if it already exists */
    unlink(so_name.sun_path);

    server(so, &so_name);
  } else
    client(so, &so_name, argv[2]);

  return 0;
}

void server(int l_so, struct sockaddr_un *so_name)
{
  int rc;
  int so;

  /* Bind socket to socket name ('file path') */
  /* What happens if we pass &so_name? */
  rc = bind(l_so, so_name, sizeof(struct sockaddr_un));
  /* The preceding call generates a compiler warning because we did
   *  not cast the struct type, see the connect call in client() for
   *  how to do it right.
   */
  if (rc == -1) {
    perror("bind");
    return;
  }
  
  /* Listen for connections */
  rc = listen(l_so, 5);
  if (rc == -1) {
    perror("listen");
    return;
  }

  /* Wait for a connection... */
  for (;;) {
    /* Block until we get a connection, then accept it,
     * creating a new socket for that connection.
     */
    so = accept(l_so, NULL, NULL);
    if (so == -1) {
      perror("accept");
      return; 			/* How about break instead? */
    }

    char *buf = malloc(BUF_SIZE);

    /* We want to read at least once; do-while is useful for this */
    do {
      rc = read(so, buf, BUF_SIZE); /* Is it safe? */

      if (rc > 0) {
	printf("%s\n", buf);
      } else if (rc == 0) {
	printf("Client went away\n");
      } else
	perror("read");

    } while (rc > 0);

    /* Are we missing something? */
  }

  close(l_so);
}

void client(int so, struct sockaddr_un *so_name, char *msg)
{
  int rc;

  /* Notice the cast here. */
  rc = connect(so, (const struct sockaddr*)so_name, sizeof(struct sockaddr_un));
  if (rc == -1) {
    perror("connect");
    return;
  }

  /* Write works on sockets as well as files: */
  rc = write(so, msg, strlen(msg));
  if (rc == -1) {
    perror("write");
    return;
  }

  /* Properly end the connection. */
  close(so);
}
