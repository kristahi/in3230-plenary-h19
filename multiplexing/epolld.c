#include <stdlib.h> 		/* malloc */
#include <stdio.h>		/* printf */
#include <string.h>		/* memset, strstr, strncpy */
#include <sys/socket.h>		/* socket, bind, listen, accept */
#include <sys/epoll.h>		/* epoll */
#include <netdb.h>		/* getaddrinfo, struct addrinfo */
#include <unistd.h>		/* read, close, unlink */

#define BUF_SIZE 256

void server(int l_so, struct addrinfo *addr);
void client(int so, struct addrinfo *addr, char *msg);

int main(int argc, char *argv[])
{
  int so, rc;
  struct sockaddr_in so_name;
  struct addrinfo hints, *res;
  
  /* Create socket */
  so = socket(AF_INET, SOCK_STREAM, 0);
  if (so == -1) {
    perror("socket");
    return 1;
  }

  /* The major difference from the UNIX socket example is in how we
     configure the binding/connecting address. */

  /* Zero out name lookup hint struct */
  memset(&hints, 0, sizeof(struct addrinfo));

  /* Resolve IP address info */
  hints.ai_family = AF_INET; 	/* IPv4 */
  hints.ai_socktype = SOCK_STREAM; /* TCP */
  hints.ai_flags = AI_PASSIVE;

  /* We use getaddrinfo to do a dynamic lookup of interface addresses
   *  available. By changing the hints we pass to it, it is for example
   *  possible to automagically try IPv6 if available.
   *  Note that it is entirely possible to manually fill in the
   *  sockaddr_in struct as well.
   */

  /* Specify the port number as the first CLI argument */
  rc = getaddrinfo(NULL, argv[1], &hints, &res);
  if (rc) {
    perror("getaddrinfo");
    return 1;
  }

  if (strstr(argv[0], "epolld") != NULL) {
    server(so, res);
  } else
    client(so, res, argv[2]);

  return 0;
}

void server(int l_so, struct addrinfo *addr)
{
  int rc;
  int so;
  int epfd;
  struct epoll_event ev;

  /* Bind socket to socket address (in this case, IP address(es)) */
  rc = bind(l_so, addr->ai_addr, addr->ai_addrlen);
  if (rc == -1) {
    perror("bind");
    return;
  }
  
  /* Listen for connections */
  rc = listen(l_so, 5);
  if (rc == -1) {
    perror("listen");
    return;
  }

  /* Create the epoll file descriptor (context) */
  /* NB: note that epoll_create (without 1) is deprecated. */
  epfd = epoll_create1(0);
  if (epfd == -1) {
    perror("epoll_create1");
    return;
  }

  /* The epoll event struct gets copied when we pass it into
   *  epoll_ctl, so we can reuse the same one to register multiple
   *  descriptors.
   */
  memset(&ev, 0, sizeof(struct epoll_event));
  ev.events = EPOLLIN | EPOLLHUP;

  /* Register stdin */
  ev.data.fd = 0;
  epoll_ctl(epfd, EPOLL_CTL_ADD, 0, &ev);
  /* Register listener socket */
  ev.data.fd = l_so;
  rc = epoll_ctl(epfd, EPOLL_CTL_ADD, l_so, &ev);

  /* This is the queue where epoll_wait will place events that have
     taken place: */
  struct epoll_event *events = calloc(10, sizeof(struct epoll_event));

  /* Wait for events... */
  for (;;) {
    int num_events;
    /* Return up to 10 events at a time, timeout 1000ms: */
    num_events = epoll_wait(epfd, events, 10, 1000);
    if (num_events == -1) {
      perror("epoll_wait");
      return;
    } else if (num_events == 0) {
      /* Nothing happened. Skip rest of loop: */
      continue;
    }

    int i;
    for (i = 0; i < num_events; i++) {
      /* Stdin: */
      if (events[i].data.fd == 0) {
	char buf[BUF_SIZE];

	if (fgets(buf, BUF_SIZE, stdin) == NULL) {
	  perror("fgets");
	  continue;
	}

	int msglen = strlen(buf);
	if (msglen < 1)
	  continue;

	if (!strcmp(buf, "quit\n"))
	  return;

	/* Implementing the broadcast message here would require us
	   to track connections ourselves. Omitted for brevity. */
	continue;
      }

      /* Listener: */
      if (events[i].data.fd == l_so) {
	so = accept(l_so, NULL, NULL);
	if (so == -1) {
	  perror("accept");
	  break;
	}

	/* Add the newly accepted connection to epoll: */
	ev.data.fd = so;
	epoll_ctl(epfd, EPOLL_CTL_ADD, so, &ev);

	continue;
      }

      /* Clients: */

      uint8_t buf[BUF_SIZE];
      buf[BUF_SIZE - 1] = 0;

      rc = read(events[i].data.fd, buf, BUF_SIZE - 1);

      if (rc > 0) {
	printf("%s\n", buf);
      } else if (rc == 0) {
	printf("Client went away\n");
	close(events[i].data.fd);
      } else
	perror("read");
    }
  }

  close(l_so);
  close(epfd);
}

void client(int so, struct addrinfo *addr, char *msg)
{
  int rc;

  rc = connect(so, addr->ai_addr, addr->ai_addrlen);
  if (rc == -1) {
    perror("connect");
    return;
  }
  
  rc = write(so, msg, strlen(msg));
  if (rc == -1) {
    perror("write");
    close(so);
    return;
  }

  close(so);
}
