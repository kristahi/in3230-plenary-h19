#include <stdlib.h> 		/* malloc */
#include <stdio.h>		/* printf */
#include <string.h>		/* memset, strstr, strncpy */
#include <sys/socket.h>		/* socket, bind, listen, accept */
#include <sys/select.h>		/* select */
#include <netdb.h>		/* getaddrinfo, struct addrinfo */
#include <unistd.h>		/* read, close, unlink */

#define BUF_SIZE 256

void server(int l_so, struct addrinfo *addr);
void client(int so, struct addrinfo *addr, char *msg);

int main(int argc, char *argv[])
{
  int so, rc;
  struct sockaddr_in so_name;
  struct addrinfo hints, *res;
  
  /* Create socket */
  so = socket(AF_INET, SOCK_STREAM, 0);
  if (so == -1) {
    perror("socket");
    return 1;
  }

  /* The major difference from the UNIX socket example is in how we
     configure the binding/connecting address. */

  /* Zero out name lookup hint struct */
  memset(&hints, 0, sizeof(struct addrinfo));

  /* Resolve IP address info */
  hints.ai_family = AF_INET; 	/* IPv4 */
  hints.ai_socktype = SOCK_STREAM; /* TCP */
  hints.ai_flags = AI_PASSIVE;

  /* We use getaddrinfo to do a dynamic lookup of interface addresses
   *  available. By changing the hints we pass to it, it is for example
   *  possible to automagically try IPv6 if available.
   *  Note that it is entirely possible to manually fill in the
   *  sockaddr_in struct as well.
   */

  /* Specify the port number as the first CLI argument */
  rc = getaddrinfo(NULL, argv[1], &hints, &res);
  if (rc) {
    perror("getaddrinfo");
    return 1;
  }

  if (strstr(argv[0], "selectd") != NULL) {
    server(so, res);
  } else
    client(so, res, argv[2]);

  return 0;
}

void server(int l_so, struct addrinfo *addr)
{
  int rc;
  int so;

  /* Bind socket to socket address (in this case, IP address(es)) */
  rc = bind(l_so, addr->ai_addr, addr->ai_addrlen);
  if (rc == -1) {
    perror("bind");
    return;
  }
  
  /* Listen for connections */
  rc = listen(l_so, 5);
  if (rc == -1) {
    perror("listen");
    return;
  }

  fd_set read_fds, write_fds;
  fd_set r_fdset, w_fdset; 	/* We need copies of these */
  struct timeval timeout;
  /* Highest current fd no. */
  int hi_fd = l_so;

  /* Initialize fd sets */
  FD_ZERO(&read_fds);
  FD_ZERO(&write_fds);

  FD_SET(0, &read_fds); 	/* Watch stdin */
  FD_SET(l_so, &read_fds);	/* Watch listener */

  /* Wait for a connection... */
  for (;;) {
    /* Reset FD sets */
    memcpy(&r_fdset, &read_fds, sizeof(fd_set));
    memcpy(&w_fdset, &write_fds, sizeof(fd_set));
    timeout.tv_sec = 1; 		/* Fire timer every second */
    timeout.tv_usec = 0;
    
    rc = select(hi_fd + 1, &r_fdset, &w_fdset, NULL, &timeout);
    if (rc == -1) {
      perror("select");
      return;
    } else if (rc == 0) {
      /* Nothing happened. Skip rest of loop: */
      continue;
    }

    if (FD_ISSET(0, &r_fdset)) {
      char buf[BUF_SIZE];

      /* Read a line of input from stdin */
      if (fgets(buf, BUF_SIZE, stdin) == NULL) {
	perror("fgets");
	continue;
      }

      int msglen = strlen(buf);
      if (msglen < 1)
	continue;

      if (!strcmp(buf, "quit\n"))
	return;

      /*
       * If we didn't get told to quit, broadcast the line to all
       * connected clients.
       */
      int fd;
      for (fd = l_so + 1; fd <= hi_fd; fd++) {
	if (FD_ISSET(fd, &read_fds)) {
	  rc = write(fd, buf, msglen + 1);

	  if (rc == -1) {
	    perror("write");
	    close(fd);
	    FD_CLR(fd, &read_fds);
	  }
	}
      }
    }

    /* Someone connected to the listener socket: */
    if (FD_ISSET(l_so, &r_fdset)) {    
      so = accept(l_so, NULL, NULL);
      if (so == -1) {
	perror("accept");
	break;
      }

      /* We have to keep track of the highest used socket descriptor
	 number so we can feed it to select. */
      if (so > hi_fd)
	hi_fd = so;
      FD_SET(so, &read_fds);
    }

    /*
     * Walk the rest of the sockets we (might) be watching. Using
     * select, there isn't really a better way of doing this.
     * (We could keep a list of only the sockets we at least know
     * are actually active, though)
     */
    int fd;
    for (fd = l_so + 1; fd <= hi_fd; fd++) {
      if (FD_ISSET(fd, &r_fdset)) {
	uint8_t buf[BUF_SIZE];
	buf[BUF_SIZE - 1] = 0;

	rc = read(fd, buf, BUF_SIZE - 1);

	if (rc > 0) {
	  printf("%s\n", buf);
	} else if (rc == 0) {
	  printf("Client went away\n");
	  close(fd);
	  FD_CLR(fd, &read_fds);
	} else
	  perror("read");
      }
    }
  }

  close(l_so);
}

void client(int so, struct addrinfo *addr, char *msg)
{
  int rc;

  rc = connect(so, addr->ai_addr, addr->ai_addrlen);
  if (rc == -1) {
    perror("connect");
    return;
  }
  
  rc = write(so, msg, strlen(msg));
  if (rc == -1) {
    perror("write");
    close(so);
    return;
  }

  close(so);
}
