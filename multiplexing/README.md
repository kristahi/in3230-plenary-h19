# Third plenary session #

## I/O multiplexing ##

There are several standard ways of dealing with the very common
situation of needing to handle both input and output to a multitude of
file descriptors (which, by now, you probably have gathered also could
be entities other than actual files, such as network sockets).

### select ###

The `select` system call is going to be available more or less
anywhere you have a proper C standard library, it is quite simple, but
not very flexible nor is it very efficient.

It is limited to watching at most `FD_SETSIZE` descriptors, with this
constant commonly being set to 1024. You also have no way of knowing
which descriptors out of the set were active without iterating over
all the descriptors you watched, which obviously gets pretty
inefficient if you have a lot of descriptors.

In general, nowadays `select` is only suitable for small programs that
will only handle a handful of descriptors during their lifetime.

Source code of the example we wrote live during the session:
[live/selectd.c](live/selectd.c)
See [selectd.c](selectd.c) for an annotated, cleaner version of
the same example, which also handles standard input.

### poll and friends ###

The `poll` system call addresses some of the weaknesses of `select`, allowing you to pass an actual list of descriptors instead of
the inflexible `fd_set` bitmap. It is also very portable
(i.e. available almost anywhere), just like `select`.

However, it still has some shortcomings, especially if you need to
deal with a large number of descriptors. Therefore it has in practice
been superseded, but unfortunately the better alternatives are *not*
standardized. On Linux we have `epoll`, while on BSD and OS X there is
`kqueue` (which is even more flexible than `epoll`).

In this course we cover `epoll` since we target the Linux platform,
and this is the alternative we recommend you use when solving the
assignments. With `epoll` you can write nice, modern event-oriented
code which should hopefully make it easier to reason about the way
your program is executed.

Source code of the example we wrote live during the session:
[live/epolld.c](live/epolld.c). This example also demonstrates how we
can combine I/O multiplexing with non-blocking sockets to great effect.

See [epolld.c](selectd.c) for an annotated, cleaner version of
the same example, which also handles standard input (this time using
blocking sockets only).

### Further reading ###

If you find nitty, gritty details regarding I/O multiplexing and
operating system internals and performance tradeoffs interesting, you can read more about the
mechanisms described above at:
https://people.eecs.berkeley.edu/~sangjin/2012/12/21/epoll-vs-kqueue.html

### Running the examples ###

We have provided a makefile to compile the examples, so just run
`make` in either this directory or the `live` subdirectory.
