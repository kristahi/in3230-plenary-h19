#include <stdlib.h> 		/* malloc */
#include <stdio.h>		/* printf */
#include <string.h>		/* memset, strstr, strncpy */
#include <sys/socket.h>		/* socket, bind, listen, accept */
#include <sys/epoll.h>		/* epoll */
#include <netdb.h>		/* getaddrinfo, struct addrinfo */
#include <unistd.h>		/* read, close, unlink */

#define BUF_SIZE 256

void server(int l_so, struct addrinfo *addr);

int main(int argc, char *argv[])
{
  int so, rc;
  struct sockaddr_in so_name;
  struct addrinfo hints, *res;
  
  /* Create socket */
  so = socket(AF_INET, SOCK_STREAM, 0);
  if (so == -1) {
    perror("socket");
    return 1;
  }

  /* The major difference from the UNIX socket example is in how we
     configure the binding/connecting address. */

  /* Zero out name lookup hint struct */
  memset(&hints, 0, sizeof(struct addrinfo));

  /* Resolve IP address info */
  hints.ai_family = AF_INET; 	/* IPv4 */
  hints.ai_socktype = SOCK_STREAM; /* TCP */
  hints.ai_flags = AI_PASSIVE;

  /* We use getaddrinfo to do a dynamic lookup of interface addresses
   *  available. By changing the hints we pass to it, it is for example
   *  possible to automagically try IPv6 if available.
   *  Note that it is entirely possible to manually fill in the
   *  sockaddr_in struct as well.
   */

  /* Specify the port number as the first CLI argument */
  rc = getaddrinfo(NULL, argv[1], &hints, &res);
  if (rc) {
    perror("getaddrinfo");
    return 1;
  }

  if (strstr(argv[0], "epolld") != NULL) {
    server(so, res);
  }

  return 0;
}

void server(int l_so, struct addrinfo *addr)
{
  int rc;
  int so;
  int epfd;
  struct epoll_event ev;

  /* Bind socket to socket address (in this case, IP address(es)) */
  rc = bind(l_so, addr->ai_addr, addr->ai_addrlen);
  if (rc == -1) {
    perror("bind");
    return;
  }
  
  /* Listen for connections */
  rc = listen(l_so, 5);
  if (rc == -1) {
    perror("listen");
    return;
  }

  epfd = epoll_create1(0);
  if (epfd == -1) {
    perror("epoll_create1");
    return;
  }

  memset(&ev, 0, sizeof(struct epoll_event));
  ev.events = EPOLLIN | EPOLLHUP;

  /* We skip handling stdin (i.e. terminal input): */
  /* ev.data.fd = 0; */
  /* epoll_ctl(epfd, EPOLL_CTL_ADD, 0, &ev); */
  /* if (rc == -1) { */
  /*   perror("epoll_ctl [stdin]"); */
  /*   return; */
  /* } */

  ev.data.fd = l_so;
  rc = epoll_ctl(epfd, EPOLL_CTL_ADD, l_so, &ev);
  if (rc == -1) {
    perror("epoll_ctl [listener]");
    return;
  }

  struct epoll_event *events = calloc(10, sizeof(struct epoll_event));

  /* Wait for a connection or something else to happen... */
  for (;;) {
    int n_events;
    /* Return up to 10 events, timeout 1000ms: */

    /*
     * BUG ALERT: in the session I used the previously defined
     * "rc" variable to record the return value of epoll_wait.
     * Subsequently, that was used in the loop condition below,
     * but I reused "rc" a second time when calling read!
     * Obviously, that broke the logic controlling the loop.
     */
    n_events = epoll_wait(epfd, events, 10, 1000);
    if (n_events == -1) {
      perror("epoll_wait");
      return;
    } else if (n_events == 0) {
      /* Nothing happened. Skip rest of loop: */
      continue;
    }

    printf("%d events occurred:\n", n_events);

    int i;
    /* BUG: the original loop condition was: i < rc */
    for (i = 0; i < n_events; i++) {
      /* Added this printout to help me debug, exposing the fact the
	 loop iterated too far: */
      printf("Event %d: Flags 0x%06x fd %d.\n",
	     i, events[i].events, events[i].data.fd);

      /* Listener: */
      if (events[i].data.fd == l_so) {
	so = accept(l_so, NULL, NULL);
	if (so == -1) {
	  perror("accept");
	  break;
	}

	ev.data.fd = so;
	rc = epoll_ctl(epfd, EPOLL_CTL_ADD, so, &ev);
	if (rc == -1)
	  perror("epoll_ctl");

	continue;
      }

      /* Clients: */

      uint8_t buf[BUF_SIZE];
      buf[BUF_SIZE - 1] = 0;

      /*
       * BUG: and here I was overwriting the value of rc with
       * something else!
       */
      rc = read(events[i].data.fd, buf, BUF_SIZE - 1);

      if (rc > 0) {
	printf("%s\n", buf);
      } else if (rc == 0) {
	printf("Client went away\n");
	close(events[i].data.fd);
      } else
	perror("read");
    
    }
  }

  close(l_so);
  close(epfd);
  free(events);
}
