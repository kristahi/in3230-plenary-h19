#include <stdlib.h> 		/* malloc */
#include <stdio.h>		/* printf */
#include <string.h>		/* memset, strstr, strncpy */
#include <sys/socket.h>		/* socket, bind, listen, accept */
#include <sys/select.h>		/* select */
#include <fcntl.h>		/* fcntl */
#include <netdb.h>		/* getaddrinfo, struct addrinfo */
#include <unistd.h>		/* read, close, unlink */
#include <errno.h>		/* errno global variable + error constants */

#define BUF_SIZE 2

void server(int l_so, struct addrinfo *addr);

int main(int argc, char *argv[])
{
  int so, rc;
  struct sockaddr_in so_name;
  struct addrinfo hints, *res;
  
  /* Create socket */
  so = socket(AF_INET, SOCK_STREAM, 0);
  if (so == -1) {
    perror("socket");
    return 1;
  }

  /* The major difference from the UNIX socket example is in how we
     configure the binding/connecting address. */

  /* Zero out name lookup hint struct */
  memset(&hints, 0, sizeof(struct addrinfo));

  /* Resolve IP address info */
  hints.ai_family = AF_INET; 	/* IPv4 */
  hints.ai_socktype = SOCK_STREAM; /* TCP */
  hints.ai_flags = AI_PASSIVE;

  /* We use getaddrinfo to do a dynamic lookup of interface addresses
   *  available. By changing the hints we pass to it, it is for example
   *  possible to automagically try IPv6 if available.
   *  Note that it is entirely possible to manually fill in the
   *  sockaddr_in struct as well.
   */

  /* Specify the port number as the first CLI argument */
  rc = getaddrinfo(NULL, argv[1], &hints, &res);
  if (rc) {
    perror("getaddrinfo");
    return 1;
  }

  if (strstr(argv[0], "selectd") != NULL) {
    server(so, res);
  }

  return 0;
}

void server(int l_so, struct addrinfo *addr)
{
  int rc;
  int so;

  /* Bind socket to socket address (in this case, IP address(es)) */
  rc = bind(l_so, addr->ai_addr, addr->ai_addrlen);
  if (rc == -1) {
    perror("bind");
    return;
  }
  
  /* Listen for connections */
  rc = listen(l_so, 5);
  if (rc == -1) {
    perror("listen");
    return;
  }

  fd_set read_fds;
  fd_set rfd_set;
  int hi_fd = l_so;

  FD_ZERO(&read_fds);

  FD_SET(0, &read_fds);
  FD_SET(l_so, &read_fds);

  for (;;) {
    struct timeval timeout;
    timeout.tv_sec = 1;
    timeout.tv_usec = 0;

    memcpy(&rfd_set, &read_fds, sizeof(fd_set));

    rc = select(hi_fd + 1, &rfd_set, NULL, NULL, &timeout);

    if (rc == -1) {
      perror("select");
      return;
    } else if (rc == 0) {
      /* Nothing happened */
      continue;
    }

    if (FD_ISSET(l_so, &rfd_set)) {
      /* Accept it */
      so = accept(l_so, NULL, NULL);
      if (so == -1) {
	perror("accept");
	break;
      }

      /*
       * Here we set the newly accepted connection's socked into
       * non-blocking mode.
       * This is required to make the looping read below work
       * correctly.
       */
      fcntl(so, F_SETFL, O_NONBLOCK);

      if (so > hi_fd)
	hi_fd = so;
      FD_SET(so, &read_fds);
    }

    if (FD_ISSET(0, &rfd_set)) {
      /* Check standard input */
      /* (We skipped implementing this to save time) */
    }

    int fd;
    for (fd = l_so + 1; fd <= hi_fd; fd++) {
      if (FD_ISSET(fd, &rfd_set)) {
	/* Read from socket */

	uint8_t buf[BUF_SIZE];

	/*
	 * Here we empty the socket buffer before calling select
	 * again. 
	 */
	do {
	  rc = read(fd, buf, BUF_SIZE - 1);

	  if (rc > 0) {
	    buf[rc] = 0;
	    printf("%s\n", buf);
	  } else if (rc == 0) {
	    printf("Client went away\n");
	    close(fd);
	    FD_CLR(fd, &read_fds);
	  } else if (errno != EAGAIN)
	    /*
	     * Notice that we ignore EAGAIN, which is what happens
	     * when there is nothing more to read from a non-blocking
	     * socket.
	     */
	    perror("read");

	} while (rc > 0);
      }
    }
  }
}
