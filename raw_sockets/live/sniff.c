#include <stdio.h> 		/* printf */
#include <sys/socket.h>		/* socket */
#include <linux/if_packet.h>	/* AF_PACKET */
#include <net/ethernet.h>	/* ETH_* */
#include <arpa/inet.h>		/* htons */
#include <string.h>

#define BUF_SIZE 1600

extern void DumpHex(const void* data, size_t size);

int main(int argc, char *argv[])
{
  int so;

  /* To only receive the frames we generate in send.c: */
  int ethertype = htons(0xffff);
  /* To receive *all* frames: */
  /* int ethertype = htons(ETH_P_ALL); */

  so = socket(AF_PACKET, SOCK_RAW, ethertype);
  if (so == -1) {
    perror("socket");
    return 1;
  }

  do {
    int rc;
    char buf[BUF_SIZE];

    rc = recv(so, buf, BUF_SIZE, 0);
    if (rc == -1) {
      perror("recv");
      return 1;
    }

    printf("Received Ethernet frame [%d bytes]:\n", rc);
    DumpHex(buf, rc);
  } while (1);
  
  return 0;
}
