#include <stdlib.h>		/* free */
#include <stdio.h> 		/* printf */
#include <string.h>		/* memset */
#include <sys/socket.h>		/* socket */
#include <linux/if_packet.h>	/* AF_PACKET */
#include <net/ethernet.h>	/* ETH_* */
#include <arpa/inet.h>		/* htons */
#include <ifaddrs.h>		/* getifaddrs */

#define BUF_SIZE 1600
#define ETH_BROADCAST_ADDR {0xff, 0xff, 0xff, 0xff, 0xff, 0xff}

struct ether_frame {
    uint8_t dst_addr[6];
    uint8_t src_addr[6];
    uint8_t eth_proto[2];
    uint8_t contents[0];
} __attribute__((packed));

/*
 * Function that pretty-prints Ethernet MAC addresses.
 * Expects to find an Ethernet address in sa.
 *
 * Returns a dynamically allocated string,
 * ownership passes to the caller.
 */
char *macaddr_str(struct sockaddr_ll *sa) {
  /* 6*2 hex chars, 5 colon delimiters + NULL: */
  char *macaddr = calloc(6 * 2 + 6, sizeof(char));
  int i;

  for (i = 0; i < 6; i++) {
    /* This is a rather inefficient way of accomplishing this, but it
       does work :) */
    char *buf = strdup(macaddr);
    
    sprintf(macaddr, "%s%02hhx%s",
	    buf,
	    sa->sll_addr[i],
	    (i < 5) ? ":" : "");

    free(buf);
  }

  return macaddr;
}

int main(int argc, char *argv[])
{
  int so, rc;
  struct msghdr *msg;
  struct iovec msgvec[2];
  struct ether_frame frame_hdr;
  struct sockaddr_ll so_name;
  struct ifaddrs *ifaces, *ifp;

  /* Set up a raw AF_PACKET socket without ethertype filtering */
  so = socket(AF_PACKET, SOCK_RAW, htons(ETH_P_ALL));
  if (so == -1) {
    perror("socket");
    return 1;
  }

  /* Enumerate interfaces: */
  /* Note in man getifaddrs that this function dynamically allocates
     memory. It becomes our responsability to free it! */
  if (getifaddrs(&ifaces)) {
    perror("getifaddrs");
    return 1;
  }

  /* Walk the list looking for ifaces interesting to us */
  printf("Interface list:\n");
  for (ifp = ifaces; ifp != NULL; ifp = ifp->ifa_next) {
    /* We make certain that the ifa_addr member is actually set: */
    if (ifp->ifa_addr != NULL && ifp->ifa_addr->sa_family == AF_PACKET) {
      /* Copy the address info into our temp. variable */
      memcpy(&so_name, (struct sockaddr_ll*)ifp->ifa_addr, sizeof(struct sockaddr_ll));
      char *addr_str = macaddr_str(&so_name);

      printf("%s\t%s\n",
	     /* Ternary statement to handle any potentially unset names: */
	     ifp->ifa_name != NULL ? ifp->ifa_name : "null",
	     addr_str);

      /* macaddr_str gave us ownership of the MAC string; free it */
      free(addr_str);
    }
  }
  /* After the loop, the address info of the last interface
     enumerated is stored in so_name. */

  /* Free the interface list */
  freeifaddrs(ifaces);


  /* Hardcode silly message */
  uint8_t buf[] = {0xde, 0xad, 0xbe, 0xef};

  /* Fill in Ethernet header */
  uint8_t broadcast_addr[] = ETH_BROADCAST_ADDR;
  memcpy(frame_hdr.dst_addr, broadcast_addr, 6);
  memcpy(frame_hdr.src_addr, so_name.sll_addr, 6);
  /* Match the ethertype in packet_socket.c: */
  frame_hdr.eth_proto[0] = frame_hdr.eth_proto[1] = 0xFF;

  /* Point to frame header */
  msgvec[0].iov_base = &frame_hdr;
  msgvec[0].iov_len = sizeof(struct ether_frame);
  /* Point to frame payload */
  msgvec[1].iov_base = buf;
  msgvec[1].iov_len = 4;

  /* Allocate a zeroed-out message info struct */
  msg = calloc(1, sizeof(struct msghdr));

  /* Fill out message metadata struct */
  memcpy(so_name.sll_addr, broadcast_addr, 6);
  msg->msg_name = &so_name;
  msg->msg_namelen = sizeof(struct sockaddr_ll);
  msg->msg_iovlen = 2;
  msg->msg_iov = msgvec;

  /* Construct and send message */
  rc = sendmsg(so, msg, 0);
  if (rc == -1) {
    perror("sendmsg");
    free(msg);
    return 1;
  }

  printf("Sent %d bytes on if with index: %d\n",
	 rc, so_name.sll_ifindex);

  /* Since we properly send the frame out of a "real" interface here,
     you actually have to be on a different host on the same network
     to see it with packet_socket. Try it in mininet! */

  /* Remember that we allocated this on the heap; free it */
  free(msg);

  return 0;
}
