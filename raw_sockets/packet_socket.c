#include <stdio.h> 		/* printf */
#include <sys/socket.h>		/* socket */
#include <linux/if_packet.h>	/* AF_PACKET */
#include <net/ethernet.h>	/* ETH_* */
#include <arpa/inet.h>		/* htons */

#define BUF_SIZE 1600

/* We declare a function available in another C file (compilation unit): */
/* Note that the "extern" qualifier is not actually required here
   because it is the default, but it makes it more obvious that the
   function is defined somewhere else. */
extern void DumpHex(const void* data, size_t size);

int main(int argc, char *argv[])
{
  int so, rc;
  char buf[BUF_SIZE];
  short unsigned int protocol;

  /* Just an arbitrarily picked protocol number: */
  protocol = 0xFFFF;
  /* This would receive any and all frames seen: */
  /* protocol = ETH_P_ALL; */

  /* We have to remember to pass the argument in network byte order: */
  so = socket(AF_PACKET, SOCK_RAW, htons(protocol));
  if (so == -1) {
    perror("socket");
    return 1;
  }

  /* Infinite loop which reads frames and hexdumps them: */
  do {
    rc = recv(so, buf, BUF_SIZE, 0);
    if (rc < 1) {
      perror("recv");
      return 1;
    }
  
    printf("Received Ethernet frame:\n");
    DumpHex(buf, rc);
  } while (1);

  return 0;
}
